import React from 'react'

const ProjectDetails = ( props ) => {
  //console.log(props.match.params.id);
  const id = props.match.params.id;
  return (
    <div className="container section project-details">
      <div className="card z-depth-0">
        <div className="card-content">
          <span className="card-title">Project Title - { id }</span>
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quo minima mollitia voluptate distinctio sunt nesciunt, aut numquam, ex consectetur quibusdam excepturi vitae ut quos aliquam molestias quia nobis iure suscipit.</p>
        </div>

        <div className="card-action gret lighten-4 grey-text">
          <div>Posted by The Net Ninja</div>
          <div>2nd Setember, 2am</div>
        </div>

      </div>
    </div>
  )
}

export default ProjectDetails
