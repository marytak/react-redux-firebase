import React from 'react'
import ProjectSummary from './ProjectSummary'

const ProjectList = ({ projects }) => {
  return (
    <div className="project-list section">

      {/*&& = then/então
        no caso, se eu tenho um projeto ENTAO faça a função, se não, nao entra na funçao 
      */}
      
      { projects && projects.map(project => {
        return(
          <ProjectSummary project={ project } key={ project.id } />
        )
      })}

    </div>
  )
}

export default ProjectList