import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBUxiVlzwm7e8Rf70fCJQshJlkbI9mGUKw",
  authDomain: "marytak-marioplan.firebaseapp.com",
  databaseURL: "https://marytak-marioplan.firebaseio.com",
  projectId: "marytak-marioplan",
  storageBucket: "marytak-marioplan.appspot.com",
  messagingSenderId: "957023350316"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase